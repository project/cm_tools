<?php

namespace Drupal\cm_tools\Cache\Context;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Cache\Context\CalculatedCacheContextInterface;
use Drupal\Core\Cache\Context\RequestStackCacheContextBase;

/**
 * Defines the CmSessionCacheContext service, for "session keys" caching.
 *
 * Note that this doesn't vary on the *values* in the session, only their
 * existence.
 *
 * Cache context ID: 'cm-session'
 * Calculated cache context ID: 'cm-session:%name', e.g. 'cm-session:tracking'
 * to vary on the existence of the tracking key in the session.
 */
class CmSessionCacheContext extends RequestStackCacheContextBase implements CalculatedCacheContextInterface {

  /**
   * {@inheritdoc}
   */
  public static function getLabel() {
    return t('CM Tools Session');
  }

  /**
   * {@inheritdoc}
   */
  public function getContext($key = NULL) {
    $request = $this->requestStack->getCurrentRequest();
    if ($key === NULL) {
      // Check for the existence of a session at all.
      return $request->hasSession() ? '1' : '0';
    }
    else {
      // Check for the existence of a specific key.
      return ($request->hasSession() && $request->getSession()->has($key)) ? '1' : '0';
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheableMetadata($key = NULL) {
    return new CacheableMetadata();
  }

}
